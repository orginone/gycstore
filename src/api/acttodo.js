import request from "@/router/axios";

const getallacttodo = (data) => {
  return request({
    url: "/dev-api/blade-system/acttodo/getallacttodo",
    method: "GET",
    params: {
      current: data.current,
      size: data.size,
      taskid: data.taskid,
      tenantCode: data.tenantCode,
    },
  });
};

const getallfinishacttodo = (data) => {
  return request({
    url: "/dev-api/blade-system/acttodo/getallfinishacttodo",
    method: "GET",
    params: {
      current: data.current,
      size: data.size,
      taskid: data.taskid,
      tenantCode: data.tenantCode,
    },
  });
};

const getallhistoryacttodo = (data) => {
  return request({
    url: "/dev-api/blade-system/acttodo/getallhistoryacttodo",
    method: "GET",
    params: {
      current: data.current,
      size: data.size,
      taskid: data.taskid,
      tenantCode: data.tenantCode,
    },
  });
};

const getallinitiateacttodo = (data) => {
  return request({
    url: "/dev-api/blade-system/acttodo/getallmysendacttodo",
    method: "GET",
    params: {
      current: data.current,
      size: data.size,
      taskid: data.taskid,
      tenantCode: data.tenantCode,
    },
  });
};
/**
 * @methods 获取待办数量
 * @param {*} data
 */
const getTodoNumber = () => {
  return request({
    url: "/dev-api/blade-system/acttodo/getallacttodonum",
    method: "GET",
  });
};

// 获取我的消息列表
const getMyMessages = data => {
  return request({
    url: '/dev-api/asset-message/message/receiver/list',
    method: 'GET',
    params: {
      current: data.current,
      size: data.size,
      title: data.title,
      content: data.content
    }
  })
}
// 获取我的消息(单条)
const getMyMessage = data => {
  return request({
    url: '/dev-api/asset-message/message/receiver/details',
    method: 'GET',
    params: {
      id: data.id
    }
  })
}
// 获取未读消息数量
const getUnReadNumber = () => {
  return request({
    url: '/dev-api/asset-message/message/receiver/unread/number',
    method: 'GET',
  });
};

// 获取流程对象详情
const getProcessDefinition = () => {
  return request({
    url: '/dev-api/asset-work-order/flowable/processDefinition/workorder',
    method: 'GET'
  });
};

// 获取待办的表格配置
const getDefaultGroup = data => {
  return request({
    url: "/dev-api/asset-system/grouptenantrelations/getDefaultGroup",
    method: "POST",
    data:data
  });
};
export {
  getallacttodo,
  getallfinishacttodo,
  getallhistoryacttodo,
  getallinitiateacttodo,
  getTodoNumber,
  getMyMessages,
  getMyMessage,
  getUnReadNumber,
  getProcessDefinition,
  getDefaultGroup
};
