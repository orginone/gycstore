export const organizationOptions = [
  {
    value: 10,
    label: "公司制企业",
  },
  {
    value: 11,
    label: "国有独资企业",
  },
  {
    value: 12,
    label: "其他有限责任公司",
  },
  {
    value: 13,
    label: "上市股份有限公司 股票代码",
  },
  {
    value: 14,
    label: "非上市股份有限公司",
  },
  {
    value: 20,
    label: "公司制企业",
  },
  {
    value: 21,
    label: "非公司制独资企业",
  },
  {
    value: 22,
    label: "其他非公司制企业",
  },
  {
    value: 30,
    label: "企业化管理事业单位",
  },
  {
    value: 40,
    label: "其他",
  },
];
export const unitTypeOption = [
  {
    value: 1,
    label: "主管部门",
  },
  {
    value: 2,
    label: "二级单位",
  },
  {
    value: 3,
    label: "基层单位",
  },
  {
    value: 100,
    label: "政府财政事务",
  },
  {
    value: 200,
    label: "安全管理事务",
  },
];
export const accountingSystemOption = [
  {
    value: 10,
    label: "政府会计制度",
  },
  {
    value: 30,
    label: "民间非营利组织会计制度",
  },
  {
    value: 50,
    label: "军工科研事业单位会计制度",
  },
];
export const administrationLevelNameOption = [
  {
    value: 1,
    label: "正部（省）级",
  },
  {
    value: 2,
    label: "副部（省）级",
  },
  {
    value: 3,
    label: "正厅（地）级",
  },
  {
    value: 4,
    label: "副厅（地）级",
  },
  {
    value: 5,
    label: "正处（县）级",
  },
  {
    value: 6,
    label: "副处（县）级",
  },
  {
    value: 7,
    label: "正科级",
  },
  {
    value: 8,
    label: "副科级",
  },
  {
    value: 9,
    label: "股级",
  },
];
export const budgetManagementLevelOption = [
  {
    value: 1,
    label: "中央级",
  },
  {
    value: 2,
    label: "省级",
  },
  {
    value: 3,
    label: "地（市）级",
  },
  {
    value: 4,
    label: "县级",
  },
  {
    value: 5,
    label: "乡镇级",
  },
];
export const budgetUnitNameOption = [
  {
    value: 1,
    label: "一级",
  },
  {
    value: 2,
    label: "二级",
  },
  {
    value: 3,
    label: "三级",
  },
  {
    value: 4,
    label: "四级",
  },
  {
    value: 5,
    label: "五级",
  },
  {
    value: 6,
    label: "六级",
  },
  {
    value: 7,
    label: "七级",
  },
];
export const fundSupplyModeNameOption = [
  {
    value: "全额",
    label: "全额",
  },
  {
    value: "差额",
    label: "差额",
  },
  {
    value: "自收自支",
    label: "自收自支",
  },
  {
    value: "其他",
    label: "其他",
  },
];
export const unitBasicPropertyOption = [
  {
    value: "11",
    label: "共产党机关",
  },
  {
    value: "12",
    label: "政府机关",
  },
  {
    value: "13",
    label: "人大机关",
  },
  {
    value: "14",
    label: "政协机关",
  },
  {
    value: "15",
    label: "群众团体",
  },
  {
    value: "16",
    label: "民主党派",
  },
  {
    value: "17",
    label: "政法机关",
  },
  {
    value: "21",
    label: "参照公务员法管理的事业单位",
  },
  {
    value: "22",
    label: "财政补助事业单位",
  },
  {
    value: "23",
    label: "经费自理事业单位",
  },
  {
    value: "31",
    label: "社会团体及其他",
  },
];
export const unitReformOption = [
  {
    value: "10",
    label: "行政类事业单位",
  },
  {
    value: "21",
    label: "公益一类事业单位",
  },
  {
    value: "22",
    label: "公益二类事业单位",
  },
  {
    value: "23",
    label: "生产经营类事业单位",
  },
  {
    value: "90",
    label: "暂未明确类别单位",
  },
];
export const personExpenditureOption = [
  {
    value: "201",
    label: "一般公共服务支出",
  },
  {
    value: "202",
    label: "外交支出",
  },
  {
    value: "203",
    label: "国防支出",
  },
  {
    value: "204",
    label: "公共安全支出",
  },
  {
    value: "205",
    label: "教育支出",
  },
  {
    value: "206",
    label: "科学技术支出",
  },
  {
    value: "20701",
    label: "文化和旅游",
  },
  {
    value: "20702",
    label: "文物",
  },
  {
    value: "20703",
    label: "体育",
  },
  {
    value: "20706",
    label: "新闻出版电影",
  },
  {
    value: "20708",
    label: "广播电视",
  },
  {
    value: "20799",
    label: "其他文化体育与传媒支出",
  },
  {
    value: "208",
    label: "社会保障和就业支出",
  },
  {
    value: "210",
    label: "卫生健康支出",
  },
  {
    value: "211",
    label: "节能环保支出",
  },
  {
    value: "212",
    label: "城乡社区支出",
  },
  {
    value: "213",
    label: "农林水支出",
  },
  {
    value: "214",
    label: "交通运输支出",
  },
  {
    value: "215",
    label: "资源勘探信息等支出",
  },
  {
    value: "216",
    label: "商业服务业等支出",
  },
  {
    value: "217",
    label: "金融支出",
  },
  {
    value: "219",
    label: "援助其他地区支出",
  },
  {
    value: "220",
    label: "自然资源海洋气象等支出",
  },
  {
    value: "221",
    label: "住房保障支出",
  },
  {
    value: "222",
    label: "粮油物资储备支出",
  },
  {
    value: "224",
    label: "灾害防治及应急管理支出",
  },
  {
    value: "229",
    label: "其他支出",
  },
];
export const isVirtualOptions =[
  {
    value:"1",
    label: "虚单位"
  },
  {
    value:"0",
    label: "实单位"
  }
]
